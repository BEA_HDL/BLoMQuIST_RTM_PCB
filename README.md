<!--version 1.1-->
# BLoMQuIST RTM PCB

## Table of contents

[[_TOC_]]

<!--toc Resources-->
## Resources

The design files of this project and also the source of this documentation are under version control in a Git repository whose upstream is:  
<https://git.gsi.de/BEA_HDL/BLoMQuIST_RTM_PCB>  
The relevant branch is *master*.

Common code for generating the documentation output files is included as a Git submodule of the main Git repository. The upstream of the submodule is:  
<https://git.gsi.de/BEA_HDL/FPGA_Common>  
The relevant branch is *master*.

The FPGA gateware can be found in the following Git repository:  
<https://git.gsi.de/BEA_HDL/BLoFELD_Gateware>  
The relevant branch is *master*.

## 1 Introduction

The BLoMQuIST (**B**eam **Lo**ss **M**onitor **Qu**ad **I**FC **S**ervice **T**erminal) MicroTCA RTM PCB is intended to be used as a replacement for the old VME based EL501 RTM.
It offers connections to four current-to-frequency-converters (IFCs) on one side and to an MicroTCA AMC card on the other side.
It is designed to be used together with the Open Hardware AMC FMC carrier (AFC) version 4, variant "RTM" .

## 2 Releases

### 2.1 Version 1.0

<https://git.gsi.de/BEA_HDL/BLoMQuIST_RTM_PCB/-/releases/v1.0>

### 2.2 Version 1.1

<https://git.gsi.de/BEA_HDL/BLoMQuIST_RTM_PCB/-/releases/v1.1>

### 2.3 Version 1.2

<https://git.gsi.de/BEA_HDL/BLoMQuIST_RTM_PCB/-/releases/v1.2>

## 3 Measurements

### 3.1 2022-11-15

Setup: Version 1.1 in a MTCA-Crate, IFC (SN 58100150) connected via cable adapter (1 m DisplayPort cable)

#### 3.1.1 Connection via 1m flat ribbon cable

![ ](doc/img/cable_1m_pulse_p.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.1:</b> positive pulse at RS485 receiver input</div><div>&nbsp;</div>

![ ](doc/img/cable_1m_pulse_n.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.2:</b> negative pulse at RS485 receiver input</div><div>&nbsp;</div>

![ ](doc/img/cable_1m_pulse_at_RS485_receiver_output.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.3:</b> single ended pulse at RS485 receiver output</div><div>&nbsp;</div>

#### 3.1.2 Connection via 150m shielded cable

![ ](doc/img/cable_150m_pulse_p.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.4:</b> positive pulse at RS485 receiver input</div><div>&nbsp;</div>

![ ](doc/img/cable_150m_pulse_n.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.5:</b> negative pulse at RS485 receiver input</div><div>&nbsp;</div>

![ ](doc/img/cable_150m_pulse_at_RS485_receiver_output.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.6:</b> single ended pulse at RS485 receiver output</div><div>&nbsp;</div>

#### 3.1.3 IFC test function

Pulse counts per second when enabling the remote control's *test* input:

|     range |     counts |
|----------:|-----------:|
|    100 nA |       9957 |
|      1 uA |      10056 |
|     10 uA |      10101 |

The expected count value is 10000, as the *test* function should create a current of 1\% of the measurement range and an upper limit current would create a 1 MHz output signal.

The gateware configuration register *counter_interval_m1* has been set to `0773593F` for a 1 second counting window.

#### 3.1.4 IO Function tests

Outputs tests:

- *range* setting works
- *polarity* setting works
- *test* setting works

Input tests:

- *ID* reads `000`
- connecting the IFC's gasflow input to GND can be detected in the gateware
- overload: Connecting a function generator with a +/-1V, 1 Hz rectangle lets the overload LED blink with 1 Hz and can be detected in the gateware

### 3.2 2023-01-25 / 2023-03-03

Setup: Version 1.1 in a MTCA-Crate, connected to four IFCs in the ELR, Rack 45

The resistance between ID 1 and {ID return for newer IFCs, GND for older IFCs} was measured to determine the cable length.

| BLoMQuIST connector | index on D-Sub-Array | label on cable        | resistance (Ohm) | IFC version |
|:-------------------:|:--------------------:|:----------------------|-----------------:|:------------|
|          1          |          7 A         | SO6DL6I/D-Steu ELR/47 |             38.2 | new         |
|          2          |          4 A         | GHTCDI1I/D-Steu       |             53.4 | old         |
|          3          |          3 A         | GHTCDI1S/D-Steu       |             52.9 | new         |
|          4          |          1 B         | HTPDI12/STSI ELR46    |             55.9 | old         |
| measured 2023-03-03 |                      |                       |                  |             |
|          1          |         11 C         | HADDI3I/STSI ELR45    |             35.5 | old         |
|          4          |          3 B         | HTCDI2S/STSI ELR46    |             50.1 | old         |

After enabling the *test* function on all four connected IFCs, all four pulse counters return reasonable results, corresponding to about 10 kHz.

#### 3.2.1 Estimation of cable lengths

Information from Carsten Müller: cable cross section probably $`0.14 \ mm^2`$ copper

Calculating the cable length using

$`l = \frac{1}{2} \frac{R \cdot A}{\rho_{copper}}`$

with $`\rho_{copper} = 0.0178\ \Omega \ mm^2 / m`$

leads to:

| BLoMQuIST connector | estimated cable length (m) |
|:-------------------:|---------------------------:|
|          1          |                        150 |
|          2          |                        210 |
|          3          |                        208 |
|          4          |                        220 |
| measured 2023-03-03 |                            |
|          1          |                        140 |
|          4          |                        197 |

#### 3.2.2 Reading the IDs

The FPGA gateware reads the following IDs (three bits):

| BLoMQuIST connector |    ID |
|:-------------------:|------:|
|          1          | `010` |
|          2          | `010` |
|          3          | `010` |
|          4          | `010` |
| measured 2023-03-03 |       |
|          1          | `001` |
|          4          | `001` |

This was verified by measuring the resistance between the *ID* pins and the *ID return* pin (new IFCs) or the *GND* pin (old IFCs) at the remote end of the cable.

<!--newpage-->
### 3.3 2023-02-17 / 2023-03-03

Oscilloscope measurements in the ELR:

The two pulse signals (p and n) coming from an IFC (GHTCDI1S/D-Steu) via a long cable, were measured at the D-Sub-15 connector of the cable adapter PCB using two test probes, as well as the reconstructed signal at the LEMO output of the EL503 FMC.
The *Test* function of the IFC was enabled to create a test current of 1 \% of the measurement range.

![ ](doc/img/ELR_pulse_measurement.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.7:</b> Pulse p (1) and n (2) measured at the cable adapter, Lemo output of the EL503 FMC (3)</div><div>&nbsp;</div>

The frequency of the incoming pulses in the oscilloscope screenshot (see Figure [3.7](#33-2023-02-17)) can be estimated to be roughly 9.3 kHz, and all the pulses are detected successfully.

The pulse counter in the FPGA showed a frequency of 9.248 kHz, which is consistent with the oscilloscope measurement.

![ ](doc/img/ELR_pulse_measurement_zoomed_in.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.8:</b> Same setup as Figure 3.7, zoomed in</div><div>&nbsp;</div>

Figure [3.8](#33-2023-02-17) was captured using the same setup, but with a zoomed in time scale.
It shows the pulse shape at the end of a cable with a length of roughly 208 m (for this value see chapter [3.2.1](#321-estimation-of-cable-lengths)).

#### 3.3.1 2023-03-03

The above measurement was repeated with two IFCs which are said to produce difficult to detect pulses (information from Plamen Boutachkov). This time, the trigger was active on the incoming pulses and the screen was averaged for 10 seconds to display possible pulses losses.

![ ](doc/img/ELR_pulse_persistent_11C.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.9:</b> Pulses from HADDI3I/STSI ELR45</div><div>&nbsp;</div>

Figure [3.9](#33-2023-02-17) shows that all the pulses that ocurred in the measurement period of 10 s (about 100000 pulses) could be detected.

![ ](doc/img/ELR_pulse_persistent_3B.png)<!--width=0.99\textwidth-->
<div align="center"><b>Figure 3.10:</b> Pulses from HTCDI2S/STSI ELR46</div><div>&nbsp;</div>

Figure [3.10](#33-2023-02-17) shows alternating patterns of double and single pulses coming from an IFC with a different pulse characteristic. All of the incoming first pulses can be detected. For the second pulse, the figure does not allow any validation, because a superposition of single and double pulses is displayed.

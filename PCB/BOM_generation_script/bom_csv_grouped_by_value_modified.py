#!/usr/bin/env python3

# Origin and Copyright: KiCad
# modified by Rene Geissler for custom output format

#
# Example python script to generate a BOM from a KiCad generic netlist
#
# Example: Sorted and Grouped CSV BOM
#

"""
    @package
    Output: CSV (comma-separated)
    Grouped By: Value
    Sorted By: Ref
    Fields: Quantity, Component Type, Value, Manufacturer, Manufacturer Part Number, Supplier, Supplier Part Number, Footprint, Footprint Type, Reference(s)

    Command line:
    python "pathToFile/bom_csv_grouped_by_value_modified.py" "%I" "%O.csv"
"""

from __future__ import print_function

import sys
# needed for finding "kicad_netlist_reader.py" and "kicad_utils.py" - the path might have to be adjusted depending on your KiCad installation
sys.path.append('/usr/share/kicad/plugins/')

# Import the KiCad python helper module and the csv formatter
import kicad_netlist_reader
import kicad_utils
import csv

# A helper function to convert a UTF8/Unicode/locale string read in netlist
# for python2 or python3 (Windows/unix)
def fromNetlistText( aText ):
    currpage = sys.stdout.encoding      #the current code page. can be none
    if currpage is None:
        return aText
    if currpage != 'utf-8':
        try:
            return aText.encode('utf-8').decode(currpage)
        except UnicodeDecodeError:
            return aText
    else:
        return aText

def myEqu(self, other):
    """myEqu is a more advanced equivalence function for components which is
    used by component grouping. Normal operation is to group components based
    on their value and footprint.

    In this example of a custom equivalency operator we compare the
    value, the part name and the footprint.
    """
    result = True
    if self.getValue() != other.getValue():
        result = False
    elif self.getPartName() != other.getPartName():
        result = False
    elif self.getFootprint() != other.getFootprint():
        result = False

    return result

# Override the component equivalence operator - it is important to do this
# before loading the netlist, otherwise all components will have the original
# equivalency operator.
kicad_netlist_reader.comp.__eq__ = myEqu

if len(sys.argv) != 3:
    print("Usage ", __file__, "<generic_netlist.xml> <output.csv>", file=sys.stderr)
    sys.exit(1)

# Generate an instance of a generic netlist, and load the netlist tree from
# the command line option. If the file doesn't exist, execution will stop
net = kicad_netlist_reader.netlist(sys.argv[1])

# Open a file to write to, if the file cannot be opened output to stdout
# instead
try:
    f = kicad_utils.open_file_write(sys.argv[2], 'w')
except IOError:
    e = "Can't open output file for writing: " + sys.argv[2]
    print( __file__, ":", e, sys.stderr )
    f = sys.stdout

# subset the components to those wanted in the BOM, controlled
# by <configure> block in kicad_netlist_reader.py
components = net.getInterestingComponents()

# prepend an initial 'hard coded' list and put the enchillada into list 'columns'
column_names = ['Quantity', 'Component Type', 'Value', 'Manufacturer', 'Manufacturer Part Number', 'Supplier', 'Supplier Part Number', 'Footprint', 'Footprint Type', 'Description', 'Reference(s)']
columns = ['COMPONENT TYPE', 'MANUFACTURER', 'MANUFACTURERNR', 'SUPPLIER 1', 'SUPPLIER PART NUMBER 1', 'FOOTPRINTTYPE']
# Create a new csv writer object to use as the output formatter
out = csv.writer( f, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL )

# override csv.writer's writerow() to support encoding conversion (initial encoding is utf8):
def writerow( acsvwriter, columns ):
    utf8row = []
    for col in columns:
        utf8row.append( fromNetlistText( str(col) ) )
    acsvwriter.writerow( utf8row )

writerow( out, column_names )

# Get all of the components in groups of matching parts + values
# (see kicad_netlist_reader.py)
grouped = net.groupComponents(components)

row = []

# Output component information organized by group, aka as collated:
for group in grouped:
    del row[:]
    refs = ""

    # Add the reference of every component in the group and keep a reference
    # to the component so that the other data can be filled in once per group
    for component in group:
        if len(refs) > 0:
            refs += ", "
        refs += component.getRef()
        c = component

    row.append( len(group) )
    row.append( net.getGroupField(group, columns[0]) );
    row.append( c.getValue() )
    row.append( net.getGroupField(group, columns[1]) );
    row.append( net.getGroupField(group, columns[2]) );
    row.append( net.getGroupField(group, columns[3]) );
    row.append( net.getGroupField(group, columns[4]) );
    row.append( net.getGroupFootprint(group) )
    row.append( net.getGroupField(group, columns[5]) );
    row.append( fromNetlistText( c.getDescription() ) );
    row.append( refs );

    writerow( out, row  )

f.close()
